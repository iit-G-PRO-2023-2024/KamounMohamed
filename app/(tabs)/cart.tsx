import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { ScrollView } from 'react-native-gesture-handler'
import FullImageCardArrow from '../../components/productCard/FullImageCardArrow'
import { useSelector } from 'react-redux'
import { RootState } from '../../types/reduxState/ReduxState'

const Cart = () => {
  const products = useSelector((state: RootState) => state.product.products);
  return (
    <ScrollView horizontal={true} style={styles.fullImageCardsWrapper}>
    {products.map &&
      products.map((product, index: number) => (
        <View style={styles.fullImageCardWrapper} key={index}>
          <FullImageCardArrow
            orientation="Vertical"
            title={product.title ?? ""}
            image={product.image ?? ""}
            subtitle="12 Items"
            buttonLink={`/products/${product.id}`}
          />
        </View>
      ))}
  </ScrollView>
  )
}

export default Cart

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
  },
  carouselWrapper: {
    marginBottom: 20,
    width: "100%",
    height: 188,
    paddingHorizontal: 24,
  },
  titleSectionWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 24,
  },
  fullImageCardsWrapper: {
    marginBottom: 20,
  },
  fullImageCardWrapper: {
    marginLeft: 20,
    borderRadius: 10,
    overflow: "hidden",
  },
  productCardsWrapper: {
    marginBottom: 20,
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-around",
    flexWrap: "wrap",
    overflow: "hidden",
  },
  columnWrapper: {
    gap: 20,
  },
  productCardWrapper: {
    marginBottom: 20,
    borderRadius: 10,
    overflow: "hidden",
  },})