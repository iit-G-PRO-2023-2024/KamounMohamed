import React from 'react';
import { View, ImageBackground, Text, StyleSheet } from 'react-native';
import { Svg, Path } from 'react-native-svg';

export default function Card() {
    return (
    		<View style={styles.card}>
      			<ImageBackground style={styles.image} source={{uri: /* dummy image */ 'https://dummyimage.com/144x144/000/fff.jpg'}}/>
      			<View style={styles.cardbody}>
        				<Text style={styles.title}>
          					{`Single Drawer Bedside Table`}
        				</Text>
        				{/* Vigma RN:: can be replaced with <Price size={"small"} /> */}
        				<View style={styles.price}>
          					<Text style={styles.currentprice}>
            						{`Rp 600.000`}
          					</Text>
          					<View style={styles.initialpricewrapper}>
            						<Text style={styles.initialprice}>
              							{`Rp 800.000`}
            						</Text>
            						{/* Vigma RN:: can be replaced with <Badge type={"pill"} size={"small"} /> */}
            						<View style={styles.badge}>
              							<Text style={styles.label}>
                								{`25%`}
              							</Text>
            						</View>
          					</View>
        				</View>
        				{/* Vigma RN:: can be replaced with <RatingProduct size={"small"} /> */}
        				<View style={styles.ratingProduct}>
          					{/* Vigma RN:: can be replaced with <IconsFilledStar  /> */}
          					<View style={styles.iconsFilledStar}>
            						<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="13" height="12" viewBox="0 0 13 12" fill="none" >
<Path d="M6.04 9.66508L3.11226 11.4288C2.98292 11.5111 2.8477 11.5464 2.7066 11.5346C2.56551 11.5228 2.44205 11.4758 2.33623 11.3935C2.2304 11.3112 2.1481 11.2083 2.08931 11.0849C2.03052 10.9614 2.01876 10.8232 2.05403 10.6704L2.83006 7.337L0.237421 5.0971C0.119841 4.99128 0.0463537 4.87076 0.0169587 4.73554C-0.0124364 4.60032 -0.00361785 4.46804 0.0434142 4.33871C0.0904462 4.20937 0.160994 4.10355 0.255058 4.02124C0.349122 3.93893 0.478461 3.88602 0.643073 3.86251L4.06465 3.56268L5.38743 0.423288C5.44622 0.282192 5.53735 0.17637 5.6608 0.105822C5.78426 0.035274 5.91066 0 6.04 0C6.16934 0 6.29574 0.035274 6.4192 0.105822C6.54266 0.17637 6.63378 0.282192 6.69257 0.423288L8.01535 3.56268L11.4369 3.86251C11.6015 3.88602 11.7309 3.93893 11.8249 4.02124C11.919 4.10355 11.9896 4.20937 12.0366 4.33871C12.0836 4.46804 12.0924 4.60032 12.063 4.73554C12.0336 4.87076 11.9602 4.99128 11.8426 5.0971L9.24994 7.337L10.026 10.6704C10.0612 10.8232 10.0495 10.9614 9.99069 11.0849C9.9319 11.2083 9.8496 11.3112 9.74377 11.3935C9.63795 11.4758 9.51449 11.5228 9.3734 11.5346C9.2323 11.5464 9.09708 11.5111 8.96775 11.4288L6.04 9.66508Z" fill="#FAB30F"/>
</Svg>

          					</View>
          					<Text style={styles._label}>
            						{`4.8`}
          					</Text>
        				</View>
      			</View>
      			{/* Vigma RN:: can be replaced with <Buttonfloating type={"filledCircle"} color={"light"} size={"small"} state={"default"} /> */}
      			<View style={styles.buttonfloating}>
        				{/* Vigma RN:: can be replaced with <Icon  /> */}
        				<View style={styles.icon}>
          					<View style={styles._boundingbox}/>
<Svg style={styles._vector} width="14" height="13" viewBox="0 0 14 13" fill="none" >
<Path d="M6.09998 11.8667L4.94998 10.8167C3.7722 9.7389 2.70831 8.66946 1.75831 7.60835C0.808313 6.54724 0.333313 5.37779 0.333313 4.10002C0.333313 3.05557 0.683313 2.18335 1.38331 1.48335C2.08331 0.78335 2.95554 0.43335 3.99998 0.43335C4.58887 0.43335 5.14442 0.55835 5.66665 0.80835C6.18887 1.05835 6.63331 1.40002 6.99998 1.83335C7.36665 1.40002 7.81109 1.05835 8.33331 0.80835C8.85554 0.55835 9.41109 0.43335 9.99998 0.43335C11.0444 0.43335 11.9166 0.78335 12.6166 1.48335C13.3166 2.18335 13.6666 3.05557 13.6666 4.10002C13.6666 5.37779 13.1944 6.55002 12.25 7.61668C11.3055 8.68335 10.2333 9.75557 9.03331 10.8333L7.89998 11.8667C7.64442 12.1111 7.34442 12.2333 6.99998 12.2333C6.65554 12.2333 6.35554 12.1111 6.09998 11.8667ZM6.36665 3.16668C6.04442 2.71113 5.69998 2.36391 5.33331 2.12502C4.96665 1.88613 4.5222 1.76668 3.99998 1.76668C3.33331 1.76668 2.77776 1.98891 2.33331 2.43335C1.88887 2.87779 1.66665 3.43335 1.66665 4.10002C1.66665 4.67779 1.8722 5.29168 2.28331 5.94168C2.69442 6.59168 3.18609 7.22224 3.75831 7.83335C4.33054 8.44446 4.91942 9.01668 5.52498 9.55002C6.13054 10.0833 6.6222 10.5222 6.99998 10.8667C7.37776 10.5222 7.86942 10.0833 8.47498 9.55002C9.08054 9.01668 9.66943 8.44446 10.2416 7.83335C10.8139 7.22224 11.3055 6.59168 11.7166 5.94168C12.1278 5.29168 12.3333 4.67779 12.3333 4.10002C12.3333 3.43335 12.1111 2.87779 11.6666 2.43335C11.2222 1.98891 10.6666 1.76668 9.99998 1.76668C9.47776 1.76668 9.03331 1.88613 8.66665 2.12502C8.29998 2.36391 7.95554 2.71113 7.63331 3.16668C7.55554 3.27779 7.46109 3.36113 7.34998 3.41668C7.23887 3.47224 7.1222 3.50002 6.99998 3.50002C6.87776 3.50002 6.76109 3.47224 6.64998 3.41668C6.53887 3.36113 6.44442 3.27779 6.36665 3.16668Z" fill="#09111F"/>
</Svg>

        				</View>
      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	card: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12
},
  	image: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 144,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 0,
    borderLeftWidth: 1,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: "rgba(225, 229, 235, 1)"
},
  	cardbody: {
    alignSelf: "stretch",
    flexShrink: 0,
    borderTopWidth: 0,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 12,
    borderBottomLeftRadius: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 2,
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderColor: "rgba(225, 229, 235, 1)"
},
  	title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	price: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0
},
  	currentprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	initialpricewrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through"
},
  	badge: {
    flexShrink: 0,
    backgroundColor: "rgba(215, 246, 228, 1)",
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
    borderRadius: 4
},
  	label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(8, 199, 84, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	ratingProduct: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 2,
    paddingHorizontal: 0
},
  	iconsFilledStar: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible"
},
  	_label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    top: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999
},
  	icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
},
  	_boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 1,
    bottom: 2,
    left: 1,
    overflow: "visible"
}
})