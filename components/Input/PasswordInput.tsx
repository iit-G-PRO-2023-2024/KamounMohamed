import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Svg, Path } from 'react-native-svg';

export default function PasswordInputfield() {
    return (
    		<View style={styles.PasswordInputfield}>
      			 
      			<View style={styles.iconleft}>
        				<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="18" height="14" viewBox="0 0 18 14" fill="none" >
<Path d="M2.33334 13.6667C1.87501 13.6667 1.48264 13.5035 1.15626 13.1771C0.829866 12.8507 0.666672 12.4583 0.666672 12V2C0.666672 1.54167 0.829866 1.1493 1.15626 0.822915C1.48264 0.496527 1.87501 0.333332 2.33334 0.333332H15.6667C16.125 0.333332 16.5174 0.496527 16.8438 0.822915C17.1701 1.1493 17.3333 1.54167 17.3333 2V12C17.3333 12.4583 17.1701 12.8507 16.8438 13.1771C16.5174 13.5035 16.125 13.6667 15.6667 13.6667H2.33334ZM9.00001 7.83333L2.33334 3.66667V12H15.6667V3.66667L9.00001 7.83333ZM9.00001 6.16667L15.6667 2H2.33334L9.00001 6.16667ZM2.33334 3.66667V2V12V3.66667Z" fill="#919BAD"/>
</Svg>

      			</View>
      			<View style={styles.inputvaluewrapper}>
        				<Text style={styles.inputvalue}>
          					{`your@email.com`}
        				</Text>
      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	PasswordInputfield: {
    alignSelf: "stretch",
    flexShrink: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12
},
  	iconleft: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
 
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 2,
    bottom: 3,
    left: 2,
    overflow: "visible"
},
  	inputvaluewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 2,
    paddingHorizontal: 4
},
  	inputvalue: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    textAlign: "left",
    color: "rgba(145, 155, 173, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
}
})